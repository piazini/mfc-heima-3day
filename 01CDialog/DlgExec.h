#if !defined(AFX_DLGEXEC_H__028ACEE1_2F55_4A22_A569_DBDFC2C3A3E4__INCLUDED_)
#define AFX_DLGEXEC_H__028ACEE1_2F55_4A22_A569_DBDFC2C3A3E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgExec.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgExec dialog

class CDlgExec : public CDialog
{
// Construction
public:
	CDlgExec(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgExec)
	enum { IDD = IDD_EXEC };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgExec)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgExec)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGEXEC_H__028ACEE1_2F55_4A22_A569_DBDFC2C3A3E4__INCLUDED_)
