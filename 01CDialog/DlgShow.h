#if !defined(AFX_DLGSHOW_H__D9FF5175_55A3_4514_837C_BB3C3D42DEA7__INCLUDED_)
#define AFX_DLGSHOW_H__D9FF5175_55A3_4514_837C_BB3C3D42DEA7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgShow.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgShow dialog

class CDlgShow : public CDialog
{
// Construction
public:
	CDlgShow(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgShow)
	enum { IDD = IDD_SHOW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgShow)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgShow)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGSHOW_H__D9FF5175_55A3_4514_837C_BB3C3D42DEA7__INCLUDED_)
