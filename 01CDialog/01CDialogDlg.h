// 01CDialogDlg.h : header file
//

#if !defined(AFX_01CDIALOGDLG_H__9F835247_4EA0_4896_ACA6_7905B02847CF__INCLUDED_)
#define AFX_01CDIALOGDLG_H__9F835247_4EA0_4896_ACA6_7905B02847CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DlgShow.h"

/////////////////////////////////////////////////////////////////////////////
// CMy01CDialogDlg dialog

class CMy01CDialogDlg : public CDialog
{
// Construction
public:
	CMy01CDialogDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMy01CDialogDlg)
	enum { IDD = IDD_MY01CDIALOG_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy01CDialogDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMy01CDialogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CDlgShow dlg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_01CDIALOGDLG_H__9F835247_4EA0_4896_ACA6_7905B02847CF__INCLUDED_)
