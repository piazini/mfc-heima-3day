// DlgExec.cpp : implementation file
//

#include "stdafx.h"
#include "01CDialog.h"
#include "DlgExec.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgExec dialog


CDlgExec::CDlgExec(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgExec::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgExec)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgExec::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgExec)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgExec, CDialog)
	//{{AFX_MSG_MAP(CDlgExec)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgExec message handlers
