// 01CDialog.h : main header file for the 01CDIALOG application
//

#if !defined(AFX_01CDIALOG_H__ABE1B634_0265_4A5E_B3F9_E656B9028268__INCLUDED_)
#define AFX_01CDIALOG_H__ABE1B634_0265_4A5E_B3F9_E656B9028268__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMy01CDialogApp:
// See 01CDialog.cpp for the implementation of this class
//

class CMy01CDialogApp : public CWinApp
{
public:
	CMy01CDialogApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy01CDialogApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMy01CDialogApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_01CDIALOG_H__ABE1B634_0265_4A5E_B3F9_E656B9028268__INCLUDED_)
