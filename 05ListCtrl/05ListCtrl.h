// 05ListCtrl.h : main header file for the 05LISTCTRL application
//

#if !defined(AFX_05LISTCTRL_H__8679054F_AFDA_4D7D_AD5E_223FEBC08147__INCLUDED_)
#define AFX_05LISTCTRL_H__8679054F_AFDA_4D7D_AD5E_223FEBC08147__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMy05ListCtrlApp:
// See 05ListCtrl.cpp for the implementation of this class
//

class CMy05ListCtrlApp : public CWinApp
{
public:
	CMy05ListCtrlApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy05ListCtrlApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMy05ListCtrlApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_05LISTCTRL_H__8679054F_AFDA_4D7D_AD5E_223FEBC08147__INCLUDED_)
