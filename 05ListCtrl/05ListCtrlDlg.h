// 05ListCtrlDlg.h : header file
//

#if !defined(AFX_05LISTCTRLDLG_H__EF771305_D8FA_4FE4_B1E4_78D0204D3E20__INCLUDED_)
#define AFX_05LISTCTRLDLG_H__EF771305_D8FA_4FE4_B1E4_78D0204D3E20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMy05ListCtrlDlg dialog

class CMy05ListCtrlDlg : public CDialog
{
// Construction
public:
	CMy05ListCtrlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMy05ListCtrlDlg)
	enum { IDD = IDD_MY05LISTCTRL_DIALOG };
	CEdit	m_edit3;
	CEdit	m_edit2;
	CEdit	m_edit1;
	CListCtrl	m_list;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy05ListCtrlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMy05ListCtrlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_05LISTCTRLDLG_H__EF771305_D8FA_4FE4_B1E4_78D0204D3E20__INCLUDED_)
