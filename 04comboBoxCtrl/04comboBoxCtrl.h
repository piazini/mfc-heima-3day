// 04comboBoxCtrl.h : main header file for the 04COMBOBOXCTRL application
//

#if !defined(AFX_04COMBOBOXCTRL_H__6BE9B3FF_9633_4B7A_BE57_ED34F40CECE8__INCLUDED_)
#define AFX_04COMBOBOXCTRL_H__6BE9B3FF_9633_4B7A_BE57_ED34F40CECE8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMy04comboBoxCtrlApp:
// See 04comboBoxCtrl.cpp for the implementation of this class
//

class CMy04comboBoxCtrlApp : public CWinApp
{
public:
	CMy04comboBoxCtrlApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy04comboBoxCtrlApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMy04comboBoxCtrlApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_04COMBOBOXCTRL_H__6BE9B3FF_9633_4B7A_BE57_ED34F40CECE8__INCLUDED_)
