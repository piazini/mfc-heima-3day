// 04comboBoxCtrlDlg.h : header file
//

#if !defined(AFX_04COMBOBOXCTRLDLG_H__CB186F97_008D_43B4_8F9A_DCA0BC749CCA__INCLUDED_)
#define AFX_04COMBOBOXCTRLDLG_H__CB186F97_008D_43B4_8F9A_DCA0BC749CCA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMy04comboBoxCtrlDlg dialog

class CMy04comboBoxCtrlDlg : public CDialog
{
// Construction
public:
	CMy04comboBoxCtrlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMy04comboBoxCtrlDlg)
	enum { IDD = IDD_MY04COMBOBOXCTRL_DIALOG };
	CComboBox	m_cbx;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy04comboBoxCtrlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMy04comboBoxCtrlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeCombo2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_04COMBOBOXCTRLDLG_H__CB186F97_008D_43B4_8F9A_DCA0BC749CCA__INCLUDED_)
