// 06CTreeCtrl.h : main header file for the 06CTREECTRL application
//

#if !defined(AFX_06CTREECTRL_H__6FCA5E83_EE94_4062_B883_F96C6A87AC0D__INCLUDED_)
#define AFX_06CTREECTRL_H__6FCA5E83_EE94_4062_B883_F96C6A87AC0D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMy06CTreeCtrlApp:
// See 06CTreeCtrl.cpp for the implementation of this class
//

class CMy06CTreeCtrlApp : public CWinApp
{
public:
	CMy06CTreeCtrlApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy06CTreeCtrlApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMy06CTreeCtrlApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_06CTREECTRL_H__6FCA5E83_EE94_4062_B883_F96C6A87AC0D__INCLUDED_)
