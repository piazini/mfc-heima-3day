// 06CTreeCtrlDlg.h : header file
//

#if !defined(AFX_06CTREECTRLDLG_H__066E9AA7_43FB_4A62_886F_9F1BAB02165E__INCLUDED_)
#define AFX_06CTREECTRLDLG_H__066E9AA7_43FB_4A62_886F_9F1BAB02165E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMy06CTreeCtrlDlg dialog

class CMy06CTreeCtrlDlg : public CDialog
{
// Construction
public:
	CMy06CTreeCtrlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMy06CTreeCtrlDlg)
	enum { IDD = IDD_MY06CTREECTRL_DIALOG };
	CTreeCtrl	m_tree;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy06CTreeCtrlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMy06CTreeCtrlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	//���ؼ�����iconͼ��
	CImageList list;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_06CTREECTRLDLG_H__066E9AA7_43FB_4A62_886F_9F1BAB02165E__INCLUDED_)
