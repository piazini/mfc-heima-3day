// 06CTreeCtrlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "06CTreeCtrl.h"
#include "06CTreeCtrlDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMy06CTreeCtrlDlg dialog

CMy06CTreeCtrlDlg::CMy06CTreeCtrlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMy06CTreeCtrlDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMy06CTreeCtrlDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy06CTreeCtrlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMy06CTreeCtrlDlg)
	DDX_Control(pDX, IDC_TREE1, m_tree);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMy06CTreeCtrlDlg, CDialog)
	//{{AFX_MSG_MAP(CMy06CTreeCtrlDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, OnSelchangedTree1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMy06CTreeCtrlDlg message handlers

BOOL CMy06CTreeCtrlDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	//树控件初始化
	//1.设置图标

	//准备HICON图标
	HICON icons[4];

	icons[0] = AfxGetApp() -> LoadIcon(IDI_ICON1);
	icons[1] = AfxGetApp() -> LoadIcon(IDI_ICON2);
	icons[2] = AfxGetApp() -> LoadIcon(IDI_ICON3);
	icons[3] = AfxGetApp() -> LoadIcon(IDI_ICON4);

	//CImageList list;		//现在list是开辟到栈上(可以理解局部),执行后会丢失。
							//所以声明要放到.h头文件里

	//创建图片集合
	list.Create(16,16,ILC_COLOR32,4,4);
	//list.Create(24,24,ILC_COLOR32,4,4);
	//list.Create(30,30,ILC_COLOR32,4,4);

	//添加具体图片
	for ( int i = 0; i < 4; i++ )
	{
		list.Add(icons[i]);
	}

	//设置图片
	m_tree.SetImageList(&list,TVSIL_NORMAL);		//默认显示图标
	//m_tree.SetImageList(&list,TVSIL_STATE);			//不显示icon图标


	//2.设置节点
	HTREEITEM root = m_tree.InsertItem(TEXT("根节点"),0,0,NULL);
	HTREEITEM parent = m_tree.InsertItem(TEXT("父节点"),1,1,root);
	HTREEITEM sub1 = m_tree.InsertItem(TEXT("子节点1"),2,2,parent);
	HTREEITEM sub2 = m_tree.InsertItem(TEXT("子节点2"),3,3,parent);

	//默认选中一项
	m_tree.SelectItem(sub1);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMy06CTreeCtrlDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMy06CTreeCtrlDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMy06CTreeCtrlDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


//切换节点，被触发下面代码
void CMy06CTreeCtrlDlg::OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;

	//提示选择的节点
	HTREEITEM item = m_tree.GetSelectedItem();
	CString name = m_tree.GetItemText(item);

	MessageBox(name);

}
