// SaleSystemView.cpp : implementation of the CSaleSystemView class
//

#include "stdafx.h"
#include "SaleSystem.h"

#include "SaleSystemDoc.h"
#include "SaleSystemView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemView

IMPLEMENT_DYNCREATE(CSaleSystemView, CView)

BEGIN_MESSAGE_MAP(CSaleSystemView, CView)
	//{{AFX_MSG_MAP(CSaleSystemView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemView construction/destruction

CSaleSystemView::CSaleSystemView()
{
	// TODO: add construction code here

}

CSaleSystemView::~CSaleSystemView()
{
}

BOOL CSaleSystemView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemView drawing

void CSaleSystemView::OnDraw(CDC* pDC)
{
	CSaleSystemDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemView diagnostics

#ifdef _DEBUG
void CSaleSystemView::AssertValid() const
{
	CView::AssertValid();
}

void CSaleSystemView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSaleSystemDoc* CSaleSystemView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSaleSystemDoc)));
	return (CSaleSystemDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemView message handlers
