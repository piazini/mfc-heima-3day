; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMainFrame
LastTemplate=CFormView
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SaleSystem.h"
LastPage=0

ClassCount=13
Class1=CSaleSystemApp
Class2=CSaleSystemDoc
Class3=CSaleSystemView
Class4=CMainFrame

ResourceCount=11
Resource1=IDD_ABOUTBOX
Resource2=DIALOG_INFO
Class5=CAboutDlg
Class6=CLoginDlg
Class7=CSelectView
Class8=CDispalyView
Resource3=IDD_DIALOGBAR (English (U.S.))
Resource4=IDD_DISPALYVIEW
Resource5=IDD_FORMVIEW (English (U.S.))
Resource6=DIALOG_ADD
Class9=CUserDlg
Resource7=DIALOG_USER
Class10=CSellDlg
Resource8=DIALOG_SELL
Class11=CInfoDlg
Resource9=IDR_MAINFRAME
Class12=CAddDlg
Resource10=DIALOG_LOGIN
Class13=CDelDlg
Resource11=DIALOG_DEL

[CLS:CSaleSystemApp]
Type=0
HeaderFile=SaleSystem.h
ImplementationFile=SaleSystem.cpp
Filter=N

[CLS:CSaleSystemDoc]
Type=0
HeaderFile=SaleSystemDoc.h
ImplementationFile=SaleSystemDoc.cpp
Filter=N

[CLS:CSaleSystemView]
Type=0
HeaderFile=SaleSystemView.h
ImplementationFile=SaleSystemView.cpp
Filter=C


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame




[CLS:CAboutDlg]
Type=0
HeaderFile=SaleSystem.cpp
ImplementationFile=SaleSystem.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_MENUITEM32771
Command2=ID_MENUITEM32772
Command3=ID_MENUITEM32773
Command4=ID_MENUITEM32774
Command5=ID_MENUITEM32775
Command6=ID_MENUITEM32776
Command7=ID_APP_ABOUT
CommandCount=7

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:DIALOG_LOGIN]
Type=1
Class=CLoginDlg
ControlCount=6
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_USER,edit,1350631552
Control4=IDC_PWD,edit,1350631584
Control5=IDC_BUTTON1,button,1342242816
Control6=IDC_BUTTON2,button,1342242816

[CLS:CLoginDlg]
Type=0
HeaderFile=LoginDlg.h
ImplementationFile=LoginDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CLoginDlg

[CLS:CSelectView]
Type=0
HeaderFile=SelectView.h
ImplementationFile=SelectView.cpp
BaseClass=CTreeView
Filter=C
LastObject=CSelectView
VirtualFilter=VWC

[CLS:CDispalyView]
Type=0
HeaderFile=DispalyView.h
ImplementationFile=DispalyView.cpp
BaseClass=CFormView
Filter=D

[DLG:IDD_DISPALYVIEW]
Type=1
Class=?
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816

[DLG:IDD_FORMVIEW (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_STATIC,static,1342308352

[DLG:IDD_DIALOGBAR (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_STATIC,static,1342308352

[DLG:DIALOG_USER]
Type=1
Class=CUserDlg
ControlCount=12
Control1=IDC_STATIC,static,1342308352
Control2=IDC_USER,edit,1350633600
Control3=IDC_NAME,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_PWD,edit,1350631584
Control7=IDC_SUREPWD,edit,1350631584
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,button,1342177287
Control11=IDC_BUTTON2,button,1342242816
Control12=IDC_BUTTON3,button,1342242816

[CLS:CUserDlg]
Type=0
HeaderFile=UserDlg.h
ImplementationFile=UserDlg.cpp
BaseClass=CFormView
Filter=D
LastObject=CUserDlg
VirtualFilter=VWC

[DLG:DIALOG_SELL]
Type=1
Class=CSellDlg
ControlCount=14
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,button,1342177287
Control3=IDC_STATIC,button,1342177287
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_COMBO1,combobox,1344339971
Control8=IDC_PRICE,edit,1484849280
Control9=IDC_NUM,edit,1350631552
Control10=IDC_SELLINFO,edit,1353779332
Control11=IDC_BUTTON1,button,1342242816
Control12=IDC_BUTTON2,button,1342242816
Control13=IDC_STATIC,static,1342308352
Control14=IDC_LEFT,edit,1484849280

[CLS:CSellDlg]
Type=0
HeaderFile=SellDlg.h
ImplementationFile=SellDlg.cpp
BaseClass=CFormView
Filter=D
LastObject=IDC_COMBO1
VirtualFilter=VWC

[DLG:DIALOG_INFO]
Type=1
Class=CInfoDlg
ControlCount=2
Control1=IDC_LIST1,SysListView32,1350631425
Control2=IDC_STATIC,static,1342308352

[CLS:CInfoDlg]
Type=0
HeaderFile=InfoDlg.h
ImplementationFile=InfoDlg.cpp
BaseClass=CFormView
Filter=D
VirtualFilter=VWC

[DLG:DIALOG_ADD]
Type=1
Class=CAddDlg
ControlCount=21
Control1=IDC_STATIC,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,button,1342177287
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_PRICE1,edit,1350639744
Control8=IDC_NUM1,edit,1350639744
Control9=IDC_BUTTON1,button,1342242816
Control10=IDC_BUTTON2,button,1342242816
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_NAME2,edit,1350631552
Control15=IDC_PRICE2,edit,1350639744
Control16=IDC_NUM2,edit,1350639744
Control17=IDC_BUTTON4,button,1342242816
Control18=IDC_BUTTON5,button,1342242816
Control19=IDC_COMBO1,combobox,1344339971
Control20=IDC_STATIC,static,1342308352
Control21=IDC_C_NUM,edit,1484849152

[CLS:CAddDlg]
Type=0
HeaderFile=AddDlg.h
ImplementationFile=AddDlg.cpp
BaseClass=CFormView
Filter=D
VirtualFilter=VWC
LastObject=CAddDlg

[DLG:DIALOG_DEL]
Type=1
Class=CDelDlg
ControlCount=11
Control1=IDC_STATIC,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_COMBO1,combobox,1342242819
Control6=IDC_PRICE,edit,1484849280
Control7=IDC_NUM,edit,1350639744
Control8=IDC_BUTTON1,button,1342242816
Control9=IDC_BUTTON2,button,1342242816
Control10=IDC_STATIC,static,1342308352
Control11=IDC_LEFT,edit,1484849280

[CLS:CDelDlg]
Type=0
HeaderFile=DelDlg.h
ImplementationFile=DelDlg.cpp
BaseClass=CFormView
Filter=D
VirtualFilter=VWC
LastObject=CDelDlg

