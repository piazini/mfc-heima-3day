#if !defined(AFX_SELLDLG_H__AB65245E_9661_43B0_82A4_2614B8D896C5__INCLUDED_)
#define AFX_SELLDLG_H__AB65245E_9661_43B0_82A4_2614B8D896C5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SellDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSellDlg form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CSellDlg : public CFormView
{
protected:
	CSellDlg();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSellDlg)

// Form Data
public:
	//{{AFX_DATA(CSellDlg)
	enum { IDD = DIALOG_SELL };
	CComboBox	m_combo;
	int		m_price;
	int		m_num;
	CString	m_sellInfo;
	int		m_left;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	void OnInitialUpdate();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSellDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSellDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSellDlg)
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELLDLG_H__AB65245E_9661_43B0_82A4_2614B8D896C5__INCLUDED_)
