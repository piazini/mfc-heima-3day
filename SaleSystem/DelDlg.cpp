// DelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SaleSystem.h"
#include "DelDlg.h"

#include "InfoFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDelDlg

IMPLEMENT_DYNCREATE(CDelDlg, CFormView)

CDelDlg::CDelDlg()
	: CFormView(CDelDlg::IDD)
{
	//{{AFX_DATA_INIT(CDelDlg)
	m_num = 0;
	m_price = 0;
	m_left = 0;
	//}}AFX_DATA_INIT
}

CDelDlg::~CDelDlg()
{
}

void CDelDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDelDlg)
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	DDX_Text(pDX, IDC_NUM, m_num);
	DDX_Text(pDX, IDC_PRICE, m_price);
	DDX_Text(pDX, IDC_LEFT, m_left);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDelDlg, CFormView)
	//{{AFX_MSG_MAP(CDelDlg)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDelDlg diagnostics

#ifdef _DEBUG
void CDelDlg::AssertValid() const
{
	CFormView::AssertValid();
}

void CDelDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDelDlg message handlers

void CDelDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	//读取文件，获取商品名，给组合框添加字符串
	//需要包含#include "InfoFile.h"
	CInfoFile file;

	file.ReadDocline(); //读取商品信息
	for(list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++)
	{
		m_combo.AddString((CString)it->name.c_str());
	}

	//将第一个商品设置为默认
	m_combo.SetCurSel(0);

	//启动调用列表里第一个选项的内容
	OnSelchangeCombo1();
}

//商品名下拉列表框
void CDelDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	//获取当前选中项
	CString text;
	int index = m_combo.GetCurSel();
	m_combo.GetLBText(index,text);

	//需要包含#include "InfoFile.h"
	CInfoFile file;
	file.ReadDocline();

	for (list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++)
	{
		if(text == it->name.c_str())
		{
			m_price=it->price;
			m_left=it->num;
			m_num = 0;
			UpdateData(FALSE); //更新内容到控件
		}
	}
}

//删除按钮
void CDelDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	
	//更新控件内容到变量
	UpdateData(TRUE);

	//获取控件内容到变量
	if(m_num == 0)
	{
		MessageBox(TEXT("删除库存数不能为0"));
		return;
	}

	CString text;
	int index = m_combo.GetCurSel();
	m_combo.GetLBText(index,text);

	CString str;
	str.Format(_T("删除商品：%s \r\n单价：%d \r\n个数：%d "), text, m_price, m_num);
	MessageBox(str);

	//需要包含#include "InfoFile.h"
	CInfoFile file;
	file.ReadDocline(); //读取商品信息
	
	for(list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++)
	{
		if(text == it->name.c_str())
		{
			it->num -= m_num;
			m_left = it->num;
		}
	}
	file.WirteDocline(); //更新文件内容

	m_num = 0;
	UpdateData(FALSE);
}

//清除按钮
void CDelDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	m_combo.SetCurSel(0); //选择第0个项目
	m_num = 0;
	OnSelchangeCombo1();
}
