// SaleSystemDoc.h : interface of the CSaleSystemDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SALESYSTEMDOC_H__D50469E8_9527_4261_BFA4_00A48598BBD8__INCLUDED_)
#define AFX_SALESYSTEMDOC_H__D50469E8_9527_4261_BFA4_00A48598BBD8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSaleSystemDoc : public CDocument
{
protected: // create from serialization only
	CSaleSystemDoc();
	DECLARE_DYNCREATE(CSaleSystemDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaleSystemDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSaleSystemDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSaleSystemDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SALESYSTEMDOC_H__D50469E8_9527_4261_BFA4_00A48598BBD8__INCLUDED_)
