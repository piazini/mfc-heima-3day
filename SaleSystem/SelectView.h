#if !defined(AFX_SELECTVIEW_H__24B6B22A_F595_461A_AA72_656E4227E1C4__INCLUDED_)
#define AFX_SELECTVIEW_H__24B6B22A_F595_461A_AA72_656E4227E1C4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectView view

#include <afxcview.h>

class CSelectView : public CTreeView
{
protected:
	CSelectView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSelectView)

// Attributes
public:

// Operations
public:
	void OnInitialUpdate();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSelectView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CSelectView)
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CTreeCtrl *m_treeCtrl;		//���ؼ�
	CImageList m_imageList;		//ͼ���б�
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTVIEW_H__24B6B22A_F595_461A_AA72_656E4227E1C4__INCLUDED_)
