// SaleSystemView.h : interface of the CSaleSystemView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SALESYSTEMVIEW_H__EA13BDE0_97C6_4162_86E5_BEA1403964C0__INCLUDED_)
#define AFX_SALESYSTEMVIEW_H__EA13BDE0_97C6_4162_86E5_BEA1403964C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSaleSystemView : public CView
{
protected: // create from serialization only
	CSaleSystemView();
	DECLARE_DYNCREATE(CSaleSystemView)

// Attributes
public:
	CSaleSystemDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaleSystemView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSaleSystemView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSaleSystemView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in SaleSystemView.cpp
inline CSaleSystemDoc* CSaleSystemView::GetDocument()
   { return (CSaleSystemDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SALESYSTEMVIEW_H__EA13BDE0_97C6_4162_86E5_BEA1403964C0__INCLUDED_)
