#if !defined(AFX_LOGINDLG_H__3A12DD32_576E_41A6_8B71_397CD44BFCB2__INCLUDED_)
#define AFX_LOGINDLG_H__3A12DD32_576E_41A6_8B71_397CD44BFCB2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LoginDlg.h : header file
//


///////////////////////////////////////
// 登录窗口在管理窗口前运行，
// 将LoginDlg窗口添加到SaleSystem.h
// 的InitInstance()方法中。在管理窗口
// 前先行启动，满足登录窗口后条件后再
// 启动管理窗口。
//
// 说明编写日期：2022/06/05
//
///////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

class CLoginDlg : public CDialog
{
// Construction
public:
	CLoginDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLoginDlg)
	enum { IDD = DIALOG_LOGIN };
	CString	m_pwd;
	CString	m_user;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoginDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLoginDlg)
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	virtual BOOL OnInitDialog();
	afx_msg void OnCancelMode();
	afx_msg void OnClose();
	afx_msg void OnOK();		//防止回车登录界面退出，如删除默认的确定按钮，这行需要手动加入
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGINDLG_H__3A12DD32_576E_41A6_8B71_397CD44BFCB2__INCLUDED_)
