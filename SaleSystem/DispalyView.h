#if !defined(AFX_DISPALYVIEW_H__DC5AFADD_118E_49A0_89D0_F751200AE820__INCLUDED_)
#define AFX_DISPALYVIEW_H__DC5AFADD_118E_49A0_89D0_F751200AE820__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DispalyView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDispalyView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include <afxcview.h>

class CDispalyView : public CFormView
{
protected:
	CDispalyView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDispalyView)

// Form Data
public:
	//{{AFX_DATA(CDispalyView)
	//enum { IDD = _UNKNOWN_RESOURCE_ID_ };
	enum { IDD = IDD_DISPALYVIEW };		//IDD_DISPALYVIEW需要手动建立，
										//在左边ResourceView选项卡的Dialog文件夹里，
										//右键“插入Dialog”，并命名为IDD_DISPALYVIEW
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDispalyView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CDispalyView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CDispalyView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DISPALYVIEW_H__DC5AFADD_118E_49A0_89D0_F751200AE820__INCLUDED_)
