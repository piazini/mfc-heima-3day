// AddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SaleSystem.h"
#include "AddDlg.h"

#include "InfoFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddDlg

IMPLEMENT_DYNCREATE(CAddDlg, CFormView)

CAddDlg::CAddDlg()
	: CFormView(CAddDlg::IDD)
{
	//{{AFX_DATA_INIT(CAddDlg)
	m_name2 = _T("");
	m_num1 = 0;
	m_num2 = 0;
	m_price1 = 0;
	m_price2 = 0;
	m_c_num = 0;
	//}}AFX_DATA_INIT
}

CAddDlg::~CAddDlg()
{
}

void CAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddDlg)
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	DDX_Text(pDX, IDC_NAME2, m_name2);
	DDX_Text(pDX, IDC_NUM1, m_num1);
	DDX_Text(pDX, IDC_NUM2, m_num2);
	DDX_Text(pDX, IDC_PRICE1, m_price1);
	DDX_Text(pDX, IDC_PRICE2, m_price2);
	DDX_Text(pDX, IDC_C_NUM, m_c_num);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddDlg, CFormView)
	//{{AFX_MSG_MAP(CAddDlg)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddDlg diagnostics

#ifdef _DEBUG
void CAddDlg::AssertValid() const
{
	CFormView::AssertValid();
}

void CAddDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CAddDlg message handlers


//初始化
void CAddDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	//读取文件，获取商品名称，给组合框添加字符
	CInfoFile file;
	file.ReadDocline();

	for (list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++)
	{
		m_combo.AddString((CString)it->name.c_str());
	}
	
	//释放list
	file.ls.clear();	

	//设置列表第一个为默认显示项
	m_combo.SetCurSel(0);

	//显示第一个列表中内容
	OnSelchangeCombo1();
}

void CAddDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	
	//获取列表选中项名称
	CString text;
	int index = m_combo.GetCurSel();
	m_combo.GetLBText(index,text);

	CInfoFile file;
	file.ReadDocline();	//读取商品信息
	for (list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++ )
	{
		if (text == it->name.c_str())
		{
			m_price1 = it->price;
			m_c_num = it->num;
			m_num1=0;
			UpdateData(FALSE);	//内容更新到空间
		}
	
	}

	//释放list
	file.ls.clear();

}

void CAddDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	//更新控件内容到变量
	UpdateData(TRUE);
	
	if (m_num1 == 0)
	{
		MessageBox(TEXT("添加库存个数不能为0个"));
		return;
	}

	CString text;
	//获取当前选中项索引号
	int index = m_combo.GetCurSel();
	m_combo.GetLBText(index,text);

	CString str;
	str.Format(_T("添加了 商品：%s \r\n单价：%d \r\n个数：%d"), text, m_price1, m_num1);
	MessageBox(str);

	//数据写入到文件
	CInfoFile file;
	file.ReadDocline();

	for(list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++ )
	{
		if (text == it->name.c_str())
		{
			it->num += m_num1;
			m_c_num = it->num;
			UpdateData(FALSE);
		}
	}

	//更新内容到文件
	file.WirteDocline();
	
	//释放list
	file.ls.clear();

	m_num1 = 0;
	UpdateData(FALSE);	//更新数据到控件
}

//库存，清除按钮
void CAddDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	//清除数据
	m_num1 = 0;
	UpdateData(FALSE);	//更新数据到控件
}

void CAddDlg::OnButton4() 
{
	// TODO: Add your control notification handler code here
	
	//获取控件内容
	UpdateData(TRUE);

	if(m_num2<=0 || m_price2<=0 || m_name2.IsEmpty())
	{
		MessageBox(TEXT("输入信息有误"));
		return;
	}

	//获取文本数据
	CInfoFile file;
	file.ReadDocline(); //读取商品信息
	file.Addline(m_name2,m_num2,m_price2);	//添加商品
	file.WirteDocline(); //写入文件
	file.ls.clear(); //释放list

	//新产品插入到左边下拉列表中
	m_combo.AddString(m_name2);

	CString str;
	str.Format(_T("新产品 %s 添加成功"),m_name2);
	MessageBox(str);

	m_name2.Empty();
	m_num2=0;
	m_price2=0;
	UpdateData(FALSE);
}

//新产品，清除按钮
void CAddDlg::OnButton5() 
{
	// TODO: Add your control notification handler code here
	
	m_name2.Empty();
	m_num2=0;
	m_price2=0;
	UpdateData(FALSE);
}
