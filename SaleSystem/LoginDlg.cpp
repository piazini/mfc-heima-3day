// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SaleSystem.h"
#include "LoginDlg.h"

#include "InfoFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog


CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoginDlg)
	m_pwd = _T("");
	m_user = _T("");
	//}}AFX_DATA_INIT
}


void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoginDlg)
	DDX_Text(pDX, IDC_PWD, m_pwd);
	DDX_Text(pDX, IDC_USER, m_user);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	//{{AFX_MSG_MAP(CLoginDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_WM_CANCELMODE()
	ON_WM_CLOSE()
	ON_WM_CAPTURECHANGED()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg message handlers


//登录按钮
void CLoginDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here

	//获取最新的文本空内容
	UpdateData(TRUE);

	if (m_user.IsEmpty() || m_pwd.IsEmpty() )
	{
		MessageBox(TEXT("用户名和密码不能为空！"));
		return;
	}

	//获取文本里的用户名密码
	CInfoFile file;
	CString name,pwd;
	
	file.ReadLogin(name,pwd);

	//判断登录框里和获取到文本里的是否一致
	if( m_user == name )	//判断用户名
	{
		if ( m_pwd == pwd )		//判断密码
		{
			//用户名和密码都对，关闭登录对话框
			CDialog::OnOK();
		}
		else
		{
			MessageBox(TEXT("密码不正确！"));
		}
	}
	else
	{
		MessageBox(TEXT("用户名不正确！"));

	}
}

//退出
void CLoginDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	exit(0);
}

//初始化
BOOL CLoginDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//初始化时自动填入账号、密码
	CInfoFile file;
	CString name,pwd;
	//读取用户、密码
	file.ReadLogin(name,pwd);

	//测试读取到的内容
	//MessageBox(name);
	//MessageBox(pwd);

	//设置用户密码到文本框
	m_user = name;
	m_pwd = pwd;

	//更新内容到文本框
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::OnCancelMode() 
{
	CDialog::OnCancelMode();
	
	// TODO: Add your message handler code here

}

void CLoginDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnClose();
}

//登录框右上角X，点击时退出全部程序而不是关闭对话框
void CLoginDlg::OnCaptureChanged(CWnd *pWnd) 
{
	// TODO: Add your message handler code here
	exit(0);
	CDialog::OnCaptureChanged(pWnd);
}

//注释掉OnOK防止在登录界面回车后，界面关闭
//如删除默认确定按钮，需要手动在LoginDlg.h头文件里
//加入afx_msg void OnOK();才能编译通过
void CLoginDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	//CDialog::OnOK();
}

