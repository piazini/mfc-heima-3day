// SaleSystemDoc.cpp : implementation of the CSaleSystemDoc class
//

#include "stdafx.h"
#include "SaleSystem.h"

#include "SaleSystemDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemDoc

IMPLEMENT_DYNCREATE(CSaleSystemDoc, CDocument)

BEGIN_MESSAGE_MAP(CSaleSystemDoc, CDocument)
	//{{AFX_MSG_MAP(CSaleSystemDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemDoc construction/destruction

CSaleSystemDoc::CSaleSystemDoc()
{
	// TODO: add one-time construction code here

}

CSaleSystemDoc::~CSaleSystemDoc()
{
}

BOOL CSaleSystemDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	//设置标题；左侧标题（主标题）;副标题在Frame的OnCreatelie里设置
	SetTitle(TEXT("销售管理系统"));

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSaleSystemDoc serialization

void CSaleSystemDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemDoc diagnostics

#ifdef _DEBUG
void CSaleSystemDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSaleSystemDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemDoc commands
