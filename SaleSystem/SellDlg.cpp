// SellDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SaleSystem.h"
#include "SellDlg.h"

#include "InfoFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSellDlg

IMPLEMENT_DYNCREATE(CSellDlg, CFormView)

CSellDlg::CSellDlg()
	: CFormView(CSellDlg::IDD)
{
	//{{AFX_DATA_INIT(CSellDlg)
	m_price = 0;
	m_num = 0;
	m_sellInfo = _T("");
	m_left = 0;
	//}}AFX_DATA_INIT
}

CSellDlg::~CSellDlg()
{
}

void CSellDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSellDlg)
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	DDX_Text(pDX, IDC_PRICE, m_price);
	DDX_Text(pDX, IDC_NUM, m_num);
	DDX_Text(pDX, IDC_SELLINFO, m_sellInfo);
	DDX_Text(pDX, IDC_LEFT, m_left);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSellDlg, CFormView)
	//{{AFX_MSG_MAP(CSellDlg)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSellDlg diagnostics

#ifdef _DEBUG
void CSellDlg::AssertValid() const
{
	CFormView::AssertValid();
}

void CSellDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSellDlg message handlers



//初始化
void CSellDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类

	CInfoFile file;
	file.ReadDocline(); //读取商品信息

	for (list<msg>::iterator it=file.ls.begin(); it != file.ls.end(); it++)
	{
		m_combo.AddString((CString)it->name.c_str());
	}

	file.ls.clear(); //清空list内容
	
	//将第一个商品设为默认显示项
	m_combo.SetCurSel(0);
	
	//启动，首次更新选中的商品
	OnSelchangeCombo1();
}	

void CSellDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	CString text;
	//获取当前选中项
	int index = m_combo.GetCurSel();
	//获取选中项内容
	m_combo.GetLBText(index,text);

	CInfoFile file;
	file.ReadDocline(); //读取商品信息
	for(list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++)
	{
		if(text == it->name.c_str())
		{
			//显示单价和数量
			m_price = it->price;
			m_left = it->num;
			m_num = 0;
			UpdateData(FALSE);
		}
	}

	//清空list内容
	file.ls.clear(); 
}

//购买按钮
void CSellDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	//更新编辑框内容->变量
	UpdateData(TRUE);

	//购买数量限制
	if (m_num <= 0)
	{
		MessageBox(TEXT("商品数量不能小于0"));
		return;
	}
	//购买数量不能大于库存
	if (m_num > m_left)
	{
		MessageBox(TEXT("商品数量不能大于库存"));
		return;
	}

	//获取列表中选中的商品
	int index = m_combo.GetCurSel();
	CString name;
	m_combo.GetLBText(index,name);

	CInfoFile file;
	file.ReadDocline();
	//遍历商品
	for(list<msg>::iterator it = file.ls.begin(); it != file.ls.end(); it++)
	{
		if((CString)it->name.c_str() == name)
		{
			it->num -= m_num;
			m_left = it->num;
			
			//购买信息显示到右侧列表框中
			CString str;
			str.Format(_T("商品：%s \r\n单价：%d \r\n个数：%d \r\n总价：%d"), name, m_price, m_num, m_price*m_num);
			m_sellInfo = str;
			UpdateData(FALSE);
			MessageBox(TEXT("购买成功"));
		}
	}

	//更新文件中数据
	file.WirteDocline();
	
	//清空数据
	m_num = 0;
	m_sellInfo.Empty();
	UpdateData(FALSE);
}

//清空按钮
void CSellDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	//清空数据
	m_num=0;
	m_sellInfo.Empty();
	UpdateData(FALSE);
}
