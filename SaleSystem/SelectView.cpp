// SelectView.cpp : implementation file
//

#include "stdafx.h"
#include "SaleSystem.h"
#include "SelectView.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//报错：error LNK2001: unresolved external symbol "public: static struct CRuntimeClass const CSelectView::classCSelectView" (?classCSelectView@CSelectView@@2UCRuntimeClass@@B)
//需要手动添加下面一行,CSelectView是对话框类名
//注意添加位置不是在某个方法里
IMPLEMENT_DYNCREATE(CSelectView, CTreeView)

/////////////////////////////////////////////////////////////////////////////
// CSelectView

//IMPLEMENT_DYNCREATE(CSelectView, CTreeView)

CSelectView::CSelectView()
{
}

CSelectView::~CSelectView()
{
}


BEGIN_MESSAGE_MAP(CSelectView, CTreeView)
	//{{AFX_MSG_MAP(CSelectView)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectView drawing

void CSelectView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CSelectView diagnostics

#ifdef _DEBUG
void CSelectView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CSelectView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSelectView message handlers


//初始化，左边树控件列表
void CSelectView::OnInitialUpdate()
{
	//获取树视图中的树控件CTreeView::GetTreeCtrl
	m_treeCtrl = &GetTreeCtrl();

	//图标资源加载CWndApp::LoadIcon
	//资源ID: IDI_ICON_RE
	HICON icon = AfxGetApp()->LoadIcon(IDI_ICON_RE);

	//图片列表的创建 CImageList::Create
	//30,30 图标显示高度宽度
	//ILC_COLOR32 图标显示32w位色
	//1，1有多少图标就写多少
	m_imageList.Create(30,30,ILC_COLOR32,1,1);

	//添加列表图片CImageList::Add
	m_imageList.Add(icon);

	//树控件设置列表CTreeCtrl::SetImageList
	m_treeCtrl ->SetImageList(&m_imageList, TVSIL_NORMAL);

	//树控件设置节点CTreeCtrl::InsertItem
	m_treeCtrl->InsertItem(TEXT("个人信息"),0,0,NULL);
	m_treeCtrl->InsertItem(TEXT("销售管理"),0,0,NULL);
	m_treeCtrl->InsertItem(TEXT("库存信息"),0,0,NULL);
	m_treeCtrl->InsertItem(TEXT("库存添加"),0,0,NULL);
	m_treeCtrl->InsertItem(TEXT("库存删除"),0,0,NULL);

}



void CSelectView::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;

	//获取当前选中列表项索引
	HTREEITEM item = m_treeCtrl ->GetSelectedItem();

	//用索引获取选中的文本内容
	CString str = m_treeCtrl ->GetItemText(item);
	//MessageBox(str);

	//发送自定义信号
	if (str == TEXT("个人信息"))
	{
		//需要包含框架类头文件#include "MainFrm.h" 
		//CWnd::PostMessage 将一个消息放入窗口的消息队列
		//AfxGetMainWnd()：框架窗口对象的指针
		//AfxGetMainWnd()->GetSafeHwnd()：获取返回窗口的句柄，CWnd::GetSafeHwnd
		//NM_A：发送自定义消息
		//(WPARAM)NM_A：指定了附加的消息信息
		//(LPARAM)0：指定了附加的消息信息，此参数这里没有意义
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_A, (WPARAM)NM_A, (LPARAM)0);
	}
	else if (str == TEXT("销售管理"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_B, (WPARAM)NM_B, (LPARAM)0);
	}
	else if (str == TEXT("库存信息"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_C, (WPARAM)NM_C, (LPARAM)0);
	}
	else if (str == TEXT("库存添加"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_D, (WPARAM)NM_D, (LPARAM)0);
	}
	else if (str == TEXT("库存删除"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_E, (WPARAM)NM_E, (LPARAM)0);
	}
}
