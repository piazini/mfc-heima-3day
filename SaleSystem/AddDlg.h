#if !defined(AFX_ADDDLG_H__69FD5C2F_9825_4FF7_ACB5_AFD8756810B4__INCLUDED_)
#define AFX_ADDDLG_H__69FD5C2F_9825_4FF7_ACB5_AFD8756810B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAddDlg form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CAddDlg : public CFormView
{
protected:
	CAddDlg();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CAddDlg)

// Form Data
public:
	//{{AFX_DATA(CAddDlg)
	enum { IDD = DIALOG_ADD };
	CComboBox	m_combo;
	CString	m_name2;
	int		m_num1;
	int		m_num2;
	int		m_price1;
	int		m_price2;
	int		m_c_num;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	void OnInitialUpdate();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CAddDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CAddDlg)
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDDLG_H__69FD5C2F_9825_4FF7_ACB5_AFD8756810B4__INCLUDED_)
