//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SaleSystem.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_SALESYTYPE                  129
#define DIALOG_LOGIN                    130
#define IDI_ICON_RE                     132
#define IDI_ICON_WIN                    133
#define IDD_DISPALYVIEW                 134
#define DIALOG_USER                     136
#define DIALOG_SELL                     137
#define DIALOG_INFO                     138
#define DIALOG_ADD                      139
#define DIALOG_DEL                      140
#define IDC_USER                        1000
#define IDC_PWD                         1001
#define IDC_BUTTON1                     1002
#define IDC_BUTTON2                     1003
#define IDC_BUTTON4                     1004
#define IDC_NAME                        1005
#define IDC_BUTTON5                     1005
#define IDC_SUREPWD                     1007
#define IDC_BUTTON3                     1008
#define IDC_COMBO1                      1009
#define IDC_PRICE                       1010
#define IDC_NUM                         1011
#define IDC_SELLINFO                    1012
#define IDC_LEFT                        1013
#define IDC_LIST1                       1013
#define IDC_PRICE1                      1015
#define IDC_NUM1                        1016
#define IDC_NAME2                       1017
#define IDC_PRICE2                      1018
#define IDC_NUM2                        1019
#define IDC_C_NUM                       1020
#define ID_MENUITEM32771                32771
#define ID_MENUITEM32772                32772
#define ID_MENUITEM32773                32773
#define ID_MENUITEM32774                32774
#define ID_MENUITEM32775                32775
#define ID_MENUITEM32776                32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
