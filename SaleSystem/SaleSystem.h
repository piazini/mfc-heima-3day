// SaleSystem.h : main header file for the SALESYSTEM application
//

#if !defined(AFX_SALESYSTEM_H__1EB23D81_08B7_473F_9894_50444FEC6575__INCLUDED_)
#define AFX_SALESYSTEM_H__1EB23D81_08B7_473F_9894_50444FEC6575__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSaleSystemApp:
// See SaleSystem.cpp for the implementation of this class
//

class CSaleSystemApp : public CWinApp
{
public:
	CSaleSystemApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaleSystemApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CSaleSystemApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SALESYSTEM_H__1EB23D81_08B7_473F_9894_50444FEC6575__INCLUDED_)
