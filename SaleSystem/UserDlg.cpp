// UserDlg.cpp : implementation file
//


#include "stdafx.h"
#include "SaleSystem.h"
#include "UserDlg.h"

#include "InfoFile.h"
//#include "afx.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserDlg

IMPLEMENT_DYNCREATE(CUserDlg, CFormView)

CUserDlg::CUserDlg()
	: CFormView(CUserDlg::IDD)
{
	//{{AFX_DATA_INIT(CUserDlg)
	m_user = _T("");
	m_surePwd = _T("");
	m_pwd = _T("");
	m_name = _T("");
	//}}AFX_DATA_INIT
}

CUserDlg::~CUserDlg()
{
}

void CUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserDlg)
	DDX_Text(pDX, IDC_USER, m_user);
	DDX_Text(pDX, IDC_SUREPWD, m_surePwd);
	DDX_Text(pDX, IDC_PWD, m_pwd);
	DDX_Text(pDX, IDC_NAME, m_name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserDlg, CFormView)
	//{{AFX_MSG_MAP(CUserDlg)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserDlg diagnostics

#ifdef _DEBUG
void CUserDlg::AssertValid() const
{
	CFormView::AssertValid();
}

void CUserDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CUserDlg message handlers

// 初始化
void CUserDlg::OnInitialUpdate()
{
	//实例读取账号密码类
	CInfoFile file;
	CString name,pwd;
	file.ReadLogin(name,pwd);

	m_user = TEXT("销售员");
	m_name = name;

	UpdateData(FALSE);	//变量同步到控件上
}

//修改密码，确定按钮
void CUserDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);	//获取控件内容到变量

	//密码不能为空
	if ( m_pwd.IsEmpty() || m_surePwd.IsEmpty() )
	{
		MessageBox(TEXT("新密码或旧密码不能为空！"));
		return;
	}

	//新密码和确认密码不一致
	if (m_pwd != m_surePwd)
	{
		MessageBox(TEXT("新密码和确认密码不一致！"));
		return;
	}


	//修改密码
	CInfoFile file;
	CString name,pwd;
	file.ReadLogin(name,pwd);

	//新旧密码相同
	if (m_surePwd == pwd)
	{
		MessageBox(TEXT("新旧密码相同"));
		return;
	}
	
	//CString 转char *
	//VC60中本来就是用窄字符，教程中的转换目的就是把宽字符转换成窄字符
	CString tmp;
	CString tmp2;
	tmp = name;
	tmp2 = m_surePwd;

	//转换后密码写入，注意是将文本编辑框m_surePwd新密码写入，不是读取出来的密码写入
	file.WritePwd( tmp.GetBuffer(0), tmp2.GetBuffer(0) );
		
	MessageBox(TEXT("密码修改成功!"));

	//清空内容
	m_pwd.Empty();
	m_surePwd.Empty();
	UpdateData(FALSE);	//变量同步到编辑框控件上
}

//清除密码按钮
void CUserDlg::OnButton3() 
{
	// TODO: Add your control notification handler code here
	m_pwd.Empty();
	m_surePwd.Empty();
	UpdateData(FALSE);	//变量同步到编辑框控件上
}
