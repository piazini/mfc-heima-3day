#if !defined(AFX_USERDLG_H__00AEFDD3_CF01_4388_8870_26FBB2F61917__INCLUDED_)
#define AFX_USERDLG_H__00AEFDD3_CF01_4388_8870_26FBB2F61917__INCLUDED_

#include <afx.h>


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserDlg form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CUserDlg : public CFormView
{
protected:
	CUserDlg();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CUserDlg)

// Form Data
public:
	//{{AFX_DATA(CUserDlg)
	enum { IDD = DIALOG_USER };
	CString	m_user;
	CString	m_surePwd;
	CString	m_pwd;
	CString	m_name;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	void OnInitialUpdate();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CUserDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CUserDlg)
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERDLG_H__00AEFDD3_CF01_4388_8870_26FBB2F61917__INCLUDED_)
