#if !defined(AFX_INFODLG_H__24333D5C_03BF_4807_8069_4A34B28B7833__INCLUDED_)
#define AFX_INFODLG_H__24333D5C_03BF_4807_8069_4A34B28B7833__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CInfoDlg : public CFormView
{
protected:
	CInfoDlg();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CInfoDlg)

// Form Data
public:
	//{{AFX_DATA(CInfoDlg)
	enum { IDD = DIALOG_INFO };
	CListCtrl	m_list;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	void OnInitialUpdate();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CInfoDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CInfoDlg)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFODLG_H__24333D5C_03BF_4807_8069_4A34B28B7833__INCLUDED_)
