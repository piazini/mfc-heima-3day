#if !defined(AFX_DELDLG_H__11135B10_DF89_4D70_9605_B5B9B8977B9F__INCLUDED_)
#define AFX_DELDLG_H__11135B10_DF89_4D70_9605_B5B9B8977B9F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDelDlg form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CDelDlg : public CFormView
{
protected:
	CDelDlg();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDelDlg)

// Form Data
public:
	//{{AFX_DATA(CDelDlg)
	enum { IDD = DIALOG_DEL };
	CComboBox	m_combo;
	int		m_num;
	int		m_price;
	int		m_left;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	void OnInitialUpdate();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CDelDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CDelDlg)
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DELDLG_H__11135B10_DF89_4D70_9605_B5B9B8977B9F__INCLUDED_)
