// 02CStaticText.h : main header file for the 02CSTATICTEXT application
//

#if !defined(AFX_02CSTATICTEXT_H__DD53AC60_37C2_4CEC_8CB7_C173694CCC0D__INCLUDED_)
#define AFX_02CSTATICTEXT_H__DD53AC60_37C2_4CEC_8CB7_C173694CCC0D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMy02CStaticTextApp:
// See 02CStaticText.cpp for the implementation of this class
//

class CMy02CStaticTextApp : public CWinApp
{
public:
	CMy02CStaticTextApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy02CStaticTextApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMy02CStaticTextApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_02CSTATICTEXT_H__DD53AC60_37C2_4CEC_8CB7_C173694CCC0D__INCLUDED_)
