// 02CStaticTextDlg.h : header file
//

#if !defined(AFX_02CSTATICTEXTDLG_H__5EAAEC93_61E4_4F0E_AF70_3F57C0A0DD84__INCLUDED_)
#define AFX_02CSTATICTEXTDLG_H__5EAAEC93_61E4_4F0E_AF70_3F57C0A0DD84__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMy02CStaticTextDlg dialog

class CMy02CStaticTextDlg : public CDialog
{
// Construction
public:
	CMy02CStaticTextDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMy02CStaticTextDlg)
	enum { IDD = IDD_MY02CSTATICTEXT_DIALOG };
	CButton	m_btn;
	CStatic	m_pic;
	CStatic	m_text;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy02CStaticTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMy02CStaticTextDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_02CSTATICTEXTDLG_H__5EAAEC93_61E4_4F0E_AF70_3F57C0A0DD84__INCLUDED_)
