// 07CTabCtrlDlg.h : header file
//

#if !defined(AFX_07CTABCTRLDLG_H__BEEA0207_9633_445A_8A90_571786F40593__INCLUDED_)
#define AFX_07CTABCTRLDLG_H__BEEA0207_9633_445A_8A90_571786F40593__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TabSheet.h"
#include "Dlg1.h"
#include "Dlg2.h"

/////////////////////////////////////////////////////////////////////////////
// CMy07CTabCtrlDlg dialog

class CMy07CTabCtrlDlg : public CDialog
{
// Construction
public:
	CMy07CTabCtrlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMy07CTabCtrlDlg)
	enum { IDD = IDD_MY07CTABCTRL_DIALOG };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy07CTabCtrlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMy07CTabCtrlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	//CTabCtrl	m_tab;	//系统里只能选择CTabCtrl类名称，不能自己输入
	CTabSheet	m_tab;	//类名称VC6.0不支持在向导里修改，所以手动添加

	CDlg1 dlg1;
	CDlg2 dlg2;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_07CTABCTRLDLG_H__BEEA0207_9633_445A_8A90_571786F40593__INCLUDED_)
