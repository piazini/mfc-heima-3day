// 03EditCtrl.h : main header file for the 03EDITCTRL application
//

#if !defined(AFX_03EDITCTRL_H__94B2E534_4FD3_4810_91D9_17BED5CBFE58__INCLUDED_)
#define AFX_03EDITCTRL_H__94B2E534_4FD3_4810_91D9_17BED5CBFE58__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMy03EditCtrlApp:
// See 03EditCtrl.cpp for the implementation of this class
//

class CMy03EditCtrlApp : public CWinApp
{
public:
	CMy03EditCtrlApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy03EditCtrlApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMy03EditCtrlApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_03EDITCTRL_H__94B2E534_4FD3_4810_91D9_17BED5CBFE58__INCLUDED_)
