// 03EditCtrlDlg.h : header file
//

#if !defined(AFX_03EDITCTRLDLG_H__EC38D3EB_801C_457C_AAFC_F6FED97DBAF3__INCLUDED_)
#define AFX_03EDITCTRLDLG_H__EC38D3EB_801C_457C_AAFC_F6FED97DBAF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMy03EditCtrlDlg dialog

class CMy03EditCtrlDlg : public CDialog
{
// Construction
public:
	CMy03EditCtrlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMy03EditCtrlDlg)
	enum { IDD = IDD_MY03EDITCTRL_DIALOG };
	CEdit	m_edit2;
	CEdit	m_edit1;
	CString	m_edit3;
	CString	m_edit4;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMy03EditCtrlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMy03EditCtrlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_03EDITCTRLDLG_H__EC38D3EB_801C_457C_AAFC_F6FED97DBAF3__INCLUDED_)
