#include <afxwin.h>

class MyApp:public CWinApp	//CWinApp应用程序类
{
public:
	//程序入口
	virtual BOOL InitInstance();	
};

class MyFrame:public CFrameWnd	//窗口框架类
{
public:
	//构造函数，类被调用时，默认执行的函数
	MyFrame();

	//宏消息映射
	DECLARE_MESSAGE_MAP();
	
	//鼠标左键按下
	afx_msg void OnLButtonDown( UINT, CPoint );
	
	//键盘按下
	afx_msg void OnChar( UINT, UINT,UINT );

	//绘图宏
	afx_msg void OnPaint( );
};
