#include "mfc.h"

MyApp app;	//全局应用程序对象，有且仅有一个
 


//程序入口，详细执行代码
BOOL MyApp::InitInstance()
{
	//创建窗口
	MyFrame * frame = new MyFrame;

	//显示和更新
	frame->ShowWindow(SW_SHOWNORMAL);
	frame->UpdateWindow();

	m_pMainWnd=frame;

	return TRUE;	//返回初始化值
}

//分界宏
BEGIN_MESSAGE_MAP(MyFrame, CFrameWnd)		//开始消息映射的定义（必须用在类实现中）  
	ON_WM_LBUTTONDOWN( )	//鼠标左键按下
	ON_WM_CHAR( )		//键盘按下
	ON_WM_PAINT( )		//绘图宏
END_MESSAGE_MAP()	//结束消息映射的定义（必须用在类实现中）

//构造函数
MyFrame::MyFrame()
{
	Create(NULL, TEXT("mfc-title"));
}

//鼠标左键按下
void MyFrame::OnLButtonDown(UINT, CPoint point)
{
	/* TCHAR buf[1024];
	wsprintf(buf, TEXT("x=%d , y=%d"), point.x, point.y);
	MessageBox(buf); */

	CString str;
	str.Format(TEXT("x = %d ,,,, y = %d"), point.x, point.y);
	MessageBox(str);
}

//键盘按下
void MyFrame::OnChar(UINT key, UINT, UINT)
{
	CString str;
	str.Format(TEXT("键盘按下 %c 键"), key);
	MessageBox(str);

}

//绘图
void MyFrame::OnPaint()
{
	CPaintDC dc(this);

	//画文本
	dc.TextOut(100,100,TEXT("测试文本"));

	//画椭圆
	dc.Ellipse(10,10,100,100);
}